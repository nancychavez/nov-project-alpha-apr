from django.urls import path
from tasks.views import create_task, user_tasks

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", user_tasks, name="show_my_tasks"),
]
